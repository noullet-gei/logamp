
#include <gtk/gtk.h>
#include <cairo-pdf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>

using namespace std;
#include <string>
#include <iostream>
#include <vector>

#include "jluplot.h"
#include "layers.h"
#include "gluplot.h"
#include "nb_serial.h"

#include "process.h"

// gestion donnees
int process::wave_process_full()
{
for	( int i = 0; i < QBUF; ++i )
	Lbuf[i] = 0.0;
init_maths();
return 0;
}

// extrait 14 bits de 2 bytes (MSB = 0 sur chaque byte, little endian)
unsigned int decode16to14( unsigned char * dbuf )
{
unsigned int retval;
retval  = ( ((unsigned int)dbuf[1]) << 7 ) & 0x3F80;
retval |= ( ((unsigned int)dbuf[0]) & 0x7F );
return retval;
}

// fontion appelee periodiquement
int process::wave_process_step()
{
int i; unsigned int val;
rxcnt = nb_serial_read( (char *)rxbuf, sizeof(rxbuf)-1 );
if	( rxcnt > 0 )
	{
	for	( i = 0; i < rxcnt; ++i )
		{
		// debug byte par byte
		// printf("%c%02X", ((rxbuf[i]&0x80)?(10):' '), (unsigned int)rxbuf[i] );
		if	( rxbuf[i] & 0x80 )
			msgi = 0;	// synchronisation sur opcode
		if	( msgi < 3 )
			msgbuf[msgi++] = rxbuf[i];
		if	( msgi == 3 )
			{
			val = decode16to14( msgbuf+1 );
			if	( msgbuf[0] & 0x40 )	// message de maintenance
				printf("%02X : %u\n", msgbuf[0] & 0x7F, val );
			else	{			// donnee de mesure
				// printf("%02X : %u\n", msgbuf[0] & 0x7F, val );
				// double dd = (double)val * 3.315/(4096*4);	// val est sur 14 bits
				// printf("%-7g\n", dd );
				Lbuf[ wri & BUFMASK ] = (float)val;
				wri++;
				}
			msgi++;		// cette valeur 4 va mettre le systeme en attente d'un opcode
			}
		}
	}
return 0;
}


// la partie du process en relation avec jluplot

// layout pour les waveforms - doit etre fait apres lecture wav data et calcul spectres
void process::prep_layout( gpanel * panneau )
{
strip * curbande;
layer_f_circ * curcour;
panneau->offscreen_flag = 1;	// 1 par defaut
// marge pour les textes
panneau->mx = 60;
panneau->kq = 200.0;	// echelles en secondes, 200 ech/s
// creer le strip
curbande = new strip;	// strip avec drawpad
panneau->bandes.push_back( curbande );
// creer le layer
curcour = new layer_f_circ;	// wave a pas uniforme
curbande->courbes.push_back( curcour );
// parentizer a cause des fonctions "set"
panneau->parentize();

// configurer le strip
curbande->bgcolor.dR = 1.0;
curbande->bgcolor.dG = 0.9;
curbande->bgcolor.dB = 0.8;
curbande->Ylabel = "log";
curbande->optX = 1;
curbande->optretX = 1;
curbande->kr = (4096*4)/3.315;
// configurer le layer
curcour->set_km( 1.0 );
curcour->set_m0( 0.0 );
curcour->set_kn( 1.0 );
curcour->set_n0( 0.0 );
curcour->label = "log";
curcour->fgcolor.dR = 0.0;
curcour->fgcolor.dG = 0.0;
curcour->fgcolor.dB = 0.75;
}

// connexion du layout aux data
void process::connect_layout( gpanel * panneau )
{
// pointeurs locaux sur les layers
layer_f_circ * layLOG = NULL;
// connecter les layers de ce layout sur les buffers existants
layLOG = (layer_f_circ *)panneau->bandes[0]->courbes[0];
layLOG->V = Lbuf;
layLOG->qu = QBUF;
// layLOG->scan();
layLOG->Vmin = 0.0;
layLOG->Vmax = 16384.0;	// data 14 bits
printf("end connect layout, %d strips\n\n", panneau->bandes.size() ); fflush(stdout);
}

// la partie maths

/* formules vues sur simu LTSPICE

params de construction
.param Kb=60m
.param k1=0.036
.param k2=0.33
.param v=3.3
.param i=10u

param deduit
.param Ka={Kb*k2/k1}

la formule log :
V=max({k2*v}+{Ka}*Log10(I(V0)/i),0)

la transformation inverse :
I=-i*pow(10,(V(Vadc)-{k2*v})/{Ka})
*/

void process::init_maths()
{
Kb   = 0.060;	// le silicium
k1   = 0.036;	// R2 et R3
k2   = 0.33;	// R5 et R6
vref = 3.3;	// intervient dans le calage du zero
iref = 10e-6;
Ka   = Kb*(k2/k1);
V0 = k2*vref;
Ka = 0.532;
V0 = 1.082;
printf("Ka = %g, V0 = %g\n", Ka, V0 );
}

double process::logamp( double i )
{
double retval;
if	( i > 0.0 )
	retval = V0 + Ka * log10(i/iref);
else	retval = 0.0;
if	( retval < 0.0 )
	return 0;
return retval;
}

double process::antilogamp( double v )
{
double retval;
retval = iref * pow( 10, ( v - V0 ) / Ka );
return retval;
}

void process::printi( char * buf, unsigned int size, double val )
{
char suff;
if	( val < 1e-3 )
	{ val *= 1e6; suff = 'u'; }
else if	( val < 1.0 )
	{ val *= 1e3; suff = 'm'; }
else	suff = ' ';
snprintf( buf, size, "%.2f%c", val, suff );
}
