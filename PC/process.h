#define QBUF (1<<14)
#define BUFMASK (QBUF-1)
#define BUFSTEP	200
#define BUFSPAN 2000

class process {
public:
// buffers
float Lbuf[QBUF];
unsigned int wri;
unsigned int rdi;
unsigned char rxbuf[64];
int rxcnt;
unsigned char msgbuf[4];
unsigned int msgi;
// parametres de traitement du signal
double Kb;
double k1;
double k2;
double vref;
double iref;
double Ka;
double V0;

// constructeur
process() : wri(0), rdi(0), msgi(4) {};
// methodes
// la partie du process qui traite en memoire les waveforms
int wave_process_full();
int wave_process_step();
void init_maths();
double logamp( double i );
double antilogamp( double v );
void printi( char * buf, unsigned int size, double val );

// la partie du process en relation avec jluplot
void prep_layout( gpanel * panneau );
void connect_layout( gpanel * panneau );
};

